#EVALUATION MMI2 : mise en application jQuery
> **Avant de commencer :**
> - Vous disposez d'une heure et demie pour effectuer les consignes de l'évaluation.
> - Une fois terminé, appelez votre enseignant pour la correction de votre travail.
> - Vous trouverez sous ces consignes des méthodes jQuery qui pourront vous aider.
> - Aidez-vous de la [documentation jQuery](http://api.jquery.com/){:target="_blank"} !
> - Usez et abusez des console.log() pour vous aider.
> - Bon courage !

----------

### **Consignes de l'évaluation** :

- Remplacez l'appel de **jQuery via CDN** par un jQuery **local** et placez cet appel de manière optimale dans votre page.
- Effectuez l'ensemble des consignes suivantes dans un fichier **app.js**. Celui-ci doit exécuter du code javascript uniquement lorsque le DOM est prêt.
- Le dictionnaire est sectionné alphabétiquement par des titres avec la class **.letter-section**. Faites en sorte que la lettre soit enveloppée d'une balise **span**.
- Comptez le nombre de définitions contenues dans le document, celles-ci sont contenues dans une balise **dt**. Insérez entre parenthèses la somme totale de ces définitions après le contenu **html** de **.logo**
- Supprimez les **.letter-section** qui n'ont pas de définition.
- Lors d'un click sur une définition, récupérez la valeur de son attribut **data-slug** et allez récupérer le contenu **html** correspondant dans le sous-dossier **/definitions**.
- Insérez le contenu **html** récupéré à l'étape précédente juste après la balise **dt** correspondante.
- Faites en sorte d'afficher uniquement la ou les définitions du terme cliqué en supprimant les précédentes définitions insérées dans la page.
- Le terme **Google** n'ayant pas de définition correspondant, faites en sortes d'afficher **dynamiquement** l'erreur suivante : 
```html
<p class='dd'>Aucune définition associée à '<strong>Google</strong>' :(</p>
```
- A l'aide du plugins **[mark.js](https://markjs.io/){:target="_blank"}**, mettre en avant le terme si celui-ci est présent dans la définition.
- Une question **BONUS** est présente à la fin de la liste de définitions. Répondez directement à la suite dans **index.php**.

### **Méthodes jQuery** :

**toLowerCase()** // **next()** // **is()** // **wrapInner()** // **wrap()** // **each()** // **append()** // **prepend()** // **html()** // **text()** // **attr()** // **remove()** // **hide()** // **show()** // **$(this)** // **$.get('url',function(){ ... })** // **click(function(){ ... })** // **if(...){...}**

