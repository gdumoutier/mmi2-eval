<!doctype html>
<html class="no-js" lang="">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>MMI2 EVALUATION</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Roboto+Mono" rel="stylesheet">
</head>
<body>
<div class="layout remodal-bg">
	<header role="banner">
		<div class="wrapper" data-relative-input="true" id="scene">
			<p class="logo" data-depth="0.6">
				<a href="#" data-page="accueil">
					Dictionnaire
				</a>
			</p>
		</div>
	</header>
	<main>
		<div class="wrapper">
			<div id="pages">
				<article>
					<div>
						<p class="letter-section">a</p>
						<dl>
							<dt data-slug="adobe">Adobe</dt>
							<dt data-slug="affordance">Affordance</dt>
							<dt data-slug="android">Android</dt>
							<dt data-slug="api">API</dt>
							<dt data-slug="arpanet">Arpanet</dt>
						</dl>
						<p class="letter-section">b</p>
						<dl>
							<dt data-slug="bing">Bing</dt>
							<dt data-slug="bios">BIOS</dt>
						</dl>
						<p class="letter-section">c</p>
						<dl>
							<dt data-slug="chrome">Chrome</dt>
							<dt data-slug="css">CSS</dt>
						</dl>
						<p class="letter-section">d</p>
						<p class="letter-section">e</p>
						<dl>
							<dt data-slug="edge">Edge</dt>
							<dt data-slug="ergonomie">Ergonomie</dt>
						</dl>
						<p class="letter-section">f</p>
						<dl>
							<dt data-slug="firefox">Firefox</dt>
							<dt data-slug="framework">Framework</dt>
							<dt data-slug="ftp">FTP</dt>
						</dl>
						<p class="letter-section">g</p>
						<dl>
							<dt data-slug="google">Google</dt>
						</dl>
						<p class="letter-section">h</p>
						<dl>
							<dt data-slug="html">HTML</dt>
						</dl>
						<p class="letter-section">i</p>
						<dl>
							<dt data-slug="internet-explorer">Internet Explorer</dt>
							<dt data-slug="ios">IOS</dt>
						</dl>
						<p class="letter-section">j</p>
						<dl>
							<dt data-slug="java">Java</dt>
							<dt data-slug="javascript">Javascript</dt>
							<dt data-slug="jquery">Jquery</dt>
							<dt data-slug="json">Json</dt>
						</dl>
						<p class="letter-section">k</p>
						<p class="letter-section">l</p>
						<dl>
							<dt data-slug="linux">Linux</dt>
						</dl>
						<p class="letter-section">m</p>
						<dl>
							<dt data-slug="microsoft">Microsoft</dt>
						</dl>
						<p class="letter-section">n</p>
						<dl>
							<dt data-slug="nodejs">Node.js</dt>
							<dt data-slug="npm">NPM</dt>
						</dl>
						<p class="letter-section">o</p>
						<p class="letter-section">p</p>
						<dl>
							<dt data-slug="preprocesseur">Préprocesseur</dt>
						</dl>
						<p class="letter-section">q</p>
						<p class="letter-section">r</p>
						<p class="letter-section">s</p>
						<dl>
							<dt data-slug="sass">sass</dt>
							<dt data-slug="sql">SQL</dt>
							<dt data-slug="sass">ssh</dt>
						</dl>
						<p class="letter-section">t</p>
						<p class="letter-section">u</p>
						<dl>
							<dt data-slug="ubuntu">Ubuntu</dt>
						</dl>
						<p class="letter-section">v</p>
						<p class="letter-section">w</p>
						<p class="letter-section">x</p>
						<dl>
							<dt data-slug="xml">XML</dt>
						</dl>
						<p class="letter-section">y</p>
						<p class="letter-section">z</p>
					</div>
					<hr>
					<p>
						<strong>Expliquez en quelques mots la methode jQuery <i>parent()</i></strong> :
						<!-- Votre réponse ici -->
					</p>
					</article>
			</div>
		</div>
	</main>
	<footer>
		<div class="wrapper">
			<ul>
				<li class="social"><a href="//twitter.com"><i class="fa fa-fw fa-twitter"></i></a></li>
				<li class="social"><a href="//twitter.com"><i class="fa fa-fw fa-facebook"></i></a></li>
			</ul>
		</div>
	</footer>
</div>

<div class="remodal" data-remodal-id="contact">
	<button data-remodal-action="close" class="remodal-close"></button>
	<h1>Contact</h1>
	<p class="letter-section">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur ipsam itaque minima, nesciunt numquam, odio officia quia quo quod recusandae tempore totam voluptatem, voluptatum. Minima molestias nemo nihil perferendis ullam.</p>
	<br>
	<form action="">
		<div class="form-group"><label for="name">Nom</label><input id="name" name="name" type="text" class="form-control"></div>
		<div class="form-group"><label for="mail">Email</label><input id="mail" name="mail" type="text" class="form-control"></div>
		<div class="form-group"><label for="message">Votre message</label><textarea name="message" id="message" cols="20" rows="5" class="form-control"></textarea></div>
	</form>
	<button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
	<button data-remodal-action="confirm" class="remodal-confirm">Envoyer</button>
</div>

<script src="https://use.fontawesome.com/601a2e39a6.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

</body>
</html>